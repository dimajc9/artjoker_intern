'use strict'
class Node {
    #data;
    #left;
    #right;

    constructor() {
        this.#data = null;
        this.#left = null;
        this.#right = null;
    }

    insert(data, node) {
        node = node || this;

        if (node.#data === null) {
            node.#data = data;
            return true;
        }
        if (node.#data > data) {
            if (node.#left === null) {
                node.#left = new Node();
            }

            return this.insert(data, node.#left);
        } else if (node.#data < data) {
            if (node.#right === null) {
                node.#right = new Node();
            }

            return this.insert(data, node.#right);
        }
    }

    search(data, node) {
        node = node || this;

        if (node.#data === data) {
            return node;
        } else if (node.#data === null) {
            return null;
        }

        if (node.#data > data && !(node.#left === null && node.#right === null)) {
            return this.search(data, node.#left);
        } else if (node.#data < data && !(node.#left === null && node.#right === null)) {
            return this.search(data, node.#right);
        }
        return null;
    }

    remove(data, node) {
        node = node || this;

        const leftNode = function (node) {
            if (node.#left === null) {
                return node;
            } else if (node.#left !== null) {
                return leftNode(node.#left);
            }
        }

        if(node.#left === null && node.#right === null){
            return null;
        } else if (node === null) {
            return null;
        } else if (data > node.#data) {
            node.#right = this.remove(data, node.#right);
            return node;
        } else if (data < node.#data) {
            node.#left = this.remove(data, node.#left);
            return node;
        } else {
            if (node.#left === null) {
                node = node.#right;
                return node;
            } else if (node.#right === null) {
                node = node.#left;
                return node;
            }
            let newNode = leftNode(node.#right);
            node.#data = newNode.#data;
            node.#right = this.remove(newNode.#data, node.#right);
            return node;
        }
    }
}

let node = new Node();
let testMass = [80, 15, 23, 12, 54, 62, 75, 23, 53, 35, 123, 25, 132, 876, 45, 865, 999999];

for (let i = 0; i < testMass.length; i++) {
    node.insert(testMass[i]);
}

node.remove(12);
console.log(node);

Array.prototype.mySortOne = function () {
    let next = 0;
    let end = this.length;
    while (next < end) {
        for (let i = 0; i < this.length; i++) {
            if (this[i] > this[i + 1]) {
                let nums = this[i];
                this[i] = this[i + 1];
                this[i + 1] = nums;
            }
        }
        next++;
        for (let i = this.length; i > 0; i--) {
            if (this[i] < this[i - 1]) {
                let nums = this[i];
                this[i] = this[i - 1];
                this[i - 1] = nums;
            }
        }
        end--;
    }
    return this;
}

Array.prototype.mySortTwo = function (callback) {
    for (let i = 0; i < this.length; i++) {
        for (let j = 0; j < this.length; j++) {
            console.log()
            if (callback(this[i], this[j])) {
                let nums = this[i];
                this[i] = this[j];
                this[j] = nums;
            }
        }
    }

    return this;
}
