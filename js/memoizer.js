'use strict'
function memoizer(fun, ...obj) {
    let cache = {};
    cache[obj] = cache[obj] || {};
    return function (...n) {
        if (cache[obj][n] != undefined) {
            return cache[obj][n];
        } else {
            let result = fun(...n);
            cache[obj][n] = result;
            return result;
        }
    }
}

const numberCounterMemoizer = memoizer(function(nums, index, obj){
    index = index || 0;
    obj = obj || {};
    let mass_nums = nums.toString().split('');
    if (mass_nums.length > index) {
        obj[mass_nums[index]] = ++obj[mass_nums[index]] || 1;
        return numberCounterMemoizer(nums, ++index, obj);
    }

    return obj;
}, 'numberCounterRecusrion');

///// 4 /////
const numberOfUniqueWordsMemoizer = memoizer(function(str, index, obj){
    index = index || 0;
    obj = obj || {};
    let strings = str.replace(/\-/g, '').split(' ');

    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        return numberOfUniqueWordsMemoizer(str, ++index, obj);
    }
    let arrayOfValues = Object.values(obj);
    let counter = 0;

    for (let i = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] === 1) {
            counter += 1;
        }
    }

    return counter;
}, 'numberOfUniqueWordsRecusrion');

///// 5 /////
const numberOccurrenceOfEachWordMemoizer = memoizer(function(str, index, obj){
    index = index || 0;
    obj = obj || {};

    let strings = str.split(' ');
    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        numberOccurrenceOfEachWordMemoizer(str, ++index, obj);
    }
    return obj;
}, 'numberOccurrenceOfEachWordRecusrion')

///// 6 /////
const fibonacciNumberMemoizer = memoizer(function(numberCycles, index, numbFibonacc){
    numberCycles = numberCycles || 0;
    index = index || 0;
    numbFibonacc = numbFibonacc || [0, 1];

    if (numberCycles / 2 > index) {
        numbFibonacc[0] = numbFibonacc[0] + numbFibonacc[1], numbFibonacc[1] = numbFibonacc[0] + numbFibonacc[1];
        fibonacciNumberMemoizer(numberCycles, ++index, numbFibonacc);
    }

    return numbFibonacc;
}, 'fibonacciNumberRecusrion');

///// 8 /////
const calculationFactorialMemoizer = memoizer(function(n){
    return (n != 1) ? n * calculationFactorialMemoizer(n - 1) : 1;
}, 'calculationFactorialRecusrion');

///// 9 /////
const multiplesOfTwoMemoizer = memoizer(function(numb, index, resultSum){
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 2 == 0) {
            resultSum += massN[index];
            return multiplesOfTwoMemoizer(numb, ++index, resultSum);
        }
        else {
            resultSum = multiplesOfTwoMemoizer(numb, ++index, resultSum);
        }
    }

    return resultSum;
}, 'multiplesOfTwoRecusrion');

const multiplesOfThreeMemoizer = memoizer(function(numb, index, resultSum){
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 3 == 0) {
            resultSum += massN[index];
            return multiplesOfThreeMemoizer(numb, ++index, resultSum);
        }
        else {
            resultSum = multiplesOfThreeMemoizer(numb, ++index, resultSum);
        }
    }

    return resultSum;
}, 'multiplesOfThreeRecusrion');

const sumPositiveNumbersMemoizer = memoizer(function(numb, index, resultSum){
    index = index || 0;
    resultSum = resultSum || 0;
    if (numb.length > index) {
        if (numb[index] > 0) {
            resultSum += numb[index];
            return sumPositiveNumbersMemoizer(numb, ++index, resultSum);
        }
        else {
            resultSum = sumPositiveNumbersMemoizer(numb, ++index, resultSum);
        }
    }

    return resultSum;
}, 'sumPositiveNumbersRecusrion');

const oddNumbersMemoizer = memoizer(function(numb, index, resultSum){
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 2) {
            resultSum += massN[index];
            return oddNumbersMemoizer(numb, ++index, resultSum);
        }
        else {
            resultSum = oddNumbersMemoizer(numb, ++index, resultSum);
        }
    }

    return resultSum;
}, 'oddNumbersRecusrion');

///// 10 /////
const determinesElementsArrayMemoizer = {
    defineZeroMemoizer: memoizer(function(mass, zero, index) {
        zero = zero || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] == 0) {
                zero++;
                return determinesElementsArrayMemoizer.defineZeroMemoizer(mass, zero, ++index);
            } else {
                zero = determinesElementsArrayMemoizer.defineZeroMemoizer(mass, zero, ++index);
            }
        }

        return zero;
    }, 'defineZero'),
    takesNegativeMemoizer: memoizer(function(mass, negative, index) {
        negative = negative || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] < 0) {
                negative++;
                return determinesElementsArrayMemoizer.takesNegativeMemoizer(mass, negative, ++index);
            } else {
                negative = determinesElementsArrayMemoizer.takesNegativeMemoizer(mass, negative, ++index);
            }
        }

        return negative;
    }, 'takesNegative'),
    takesPositiveMemoizer: memoizer(function(mass, positive, index) {
        positive = positive || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] > 0) {
                positive++;
                return determinesElementsArrayMemoizer.takesPositiveMemoizer(mass, positive, ++index);
            } else {
                positive = determinesElementsArrayMemoizer.takesPositiveMemoizer(mass, positive, ++index);
            }
        }

        return positive;
    }, 'takesPositive'),
    takesPrimeNumbersMemoizer: memoizer(function(mass, primeNumbers, index){
        primeNumbers = primeNumbers || 0;
        index = index || 0;
        if (mass.length > index) {
            if (!(mass[index] % 2 && mass[index] % 3 && mass[index] % 5 && mass[index] % 7)) {
                primeNumbers++;
                return determinesElementsArrayMemoizer.takesPrimeNumbersMemoizer(mass, primeNumbers, ++index);
            } else {
                primeNumbers = determinesElementsArrayMemoizer.takesPrimeNumbersMemoizer(mass, primeNumbers, ++index);
            }
        }

        return primeNumbers;
    }, 'takesPrimeNumbers'),
}

///// 12 ////
const multiplesOfTwoDimensionalArrayMemoizer = memoizer(function(mass){
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 2 == 0) {
            result += pavedMassif[index];
        }
    }

    return result;
}, 'multiplesOfTwoDimensionalArrayRecusrion');

const multiplesOfThreeDimensionalArrayMemoizer = memoizer(function(mass){
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 3 == 0 && pavedMassif[index] !== 0) {
            result += pavedMassif[index];
        }
    }
    return result;
}, 'multiplesOfThreeDimensionalArrayRecusrion');

const sumPositiveNumbersDimensionalArrayMemoizer = memoizer(function(mass){
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);
    for (let index in pavedMassif) {
        if (pavedMassif[index] > 0) {
            result += pavedMassif[index];
        }
    }
    return result;
}, 'sumPositiveNumbersDimensionalArrayRecusrion');

const oddNumbersDimensionalArrayMemoizer = memoizer(function(mass){
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 2) {
            result += pavedMassif[index];
        }
    }
    return result;
}, 'oddNumbersDimensionalArrayRecusrion');

const determinesTheNumberElementsTheArrayDimensionalArrayMemoizer = memoizer(function(mass){
    let objElems = {
        zero: 0,
        negative: 0,
        positive: 0,
        primeNumbers: 0,
    }
    let pavedMassif = [];
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);
    for (let index in pavedMassif) {
        if (pavedMassif[index] == 0) {
            objElems.zero++;
        }
        if (pavedMassif[index] < 0) {
            objElems.negative++;
        }
        if (pavedMassif[index] > 0) {
            objElems.positive++;
        }
        if (!(pavedMassif[index] % 2 && pavedMassif[index] % 3 && pavedMassif[index] % 5 && pavedMassif[index] % 7)) {
            objElems.primeNumbers++;
        }
    }

    return objElems;
}, 'determinesTheNumberElementsTheArrayDimensionalArrayRecusrion');

///// 13 /////
const sumElementsMinMaxMemoizer = memoizer(function(min, max, sumMinMax){
    sumMinMax = sumMinMax || 0;
    if (++min <= max) {
        if (min % 3 == 0 && min > 0) {
            sumMinMax += min;
        }
        sumMinMax = sumElementsMinMaxMemoizer(min, max, sumMinMax);
    }

    return sumMinMax;;
}, 'sumElementsMinMaxRecusrion');

///// 14 /////
const theAverageOfArrayElementMemoizer = memoizer(function(mass){
    let massFlat = mass.flat();
    let resultMean = {
        even: 0,
        notEven: 0,
    }

    const parsingArray = (massFlat, counter) => {
        counter = counter || 0;
        if (massFlat.length > counter) {
            if (massFlat[counter] % 2 == 0) {
                resultMean.even += massFlat[counter] / 2;
            } else {
                resultMean.notEven += massFlat[counter] / 2;
            }

            parsingArray(massFlat, ++counter);
        }
    }
    parsingArray(massFlat);

    return resultMean;
}, 'theAverageOfArrayElementRecusrion');

///// 16 /////
let matricesOneMemoizer = [[1, 3, 7],
[3, 8, 2],
[2, 9, 0]];

let matricesTwoMemoizer = [[1, 2, 2],
[6, 2, 7],
[1, 7, 9]];

const addsTwoMatricesMemoizer = memoizer(function(matricesOneMemoizer, matricesTwoRecursion, count, result){
    count = count || 0;
    result = result || [[0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]];

    if (count < matricesOneMemoizer.length) {
        for (let index in matricesOneMemoizer[count]) {
            result[count][index] += matricesOneMemoizer[count][index] + matricesTwoRecursion[count][index];
        }

        addsTwoMatricesMemoizer(matricesOneMemoizer, matricesTwoRecursion, ++count, result);
    }

    return result;
}, 'addsTwoMatricesRecusrion');

///// 17 /////
const removeFromArrayStringMemoizer = memoizer(function(mass, index){
    index = index || 0;

    if (index++ < mass.length) {
        for (let j in mass[index]) {
            if (mass[index][j] == 0) {
                mass.splice(index, 1);
                index = 0;
                break;
            }
        }

        removeFromArrayStringMemoizer(mass, index);
    }

    return mass;
}, 'removeFromArrayStringRecusrion');

const removeFromArrayСolumnMemoizer = memoizer(function(mass, index){
    index = index || 0;
    const deleteСolumn = (j) => {
        for (let index in mass) {
            mass[index].splice(j, 1);
        }
    }

    if (index++ < mass.length) {
        for (let j in mass[index]) {
            if (mass[index][j] == 0) {
                deleteСolumn(j);
                break;
            }
        }

        removeFromArrayСolumnMemoizer(mass, index);
    }

    return mass;
}, 'removeFromArrayСolumnRecusrion');
