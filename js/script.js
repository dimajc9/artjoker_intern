'use strict'

///// 1 /////
const anagram = (firstlLine, secondLine) => {
    let string = '';

    if (firstlLine.length == secondLine.length) {
        for (let i = 0; i < firstlLine.length; i++) {
            for (let j = 0; j < secondLine.length; j++) {
                if (firstlLine[i] == secondLine[j]) {
                    string += firstlLine[i];
                    break;
                }
            }
        }
    } else {
        return false;
    }
    if (firstlLine == string) {
        return true;
    }

    return false;
}

///// 3 /////
const numberCounter = (nums) => {
    let mass_nums = nums.toString().split('');
    let obj = {};
    for (let i in mass_nums) {
        obj[mass_nums[i]] = ++obj[mass_nums[i]] || 1;
    }

    return obj;
}

///// 4 /////
const numberOfUniqueWords = (str) => {
    let strings = str.replace(/\-/g, '').split(' ');
    let obj = {};

    for (let i in strings) {
        obj[strings[i]] = ++obj[strings[i]] || 1;
    }

    let arrayOfValues = Object.values(obj);
    let numberOfUniqueWords = 0;

    for (let i = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] == 1) {
            numberOfUniqueWords += 1;
        }
    }

    return numberOfUniqueWords;
}

///// 5 /////
const numberOccurrenceOfEachWord = (str) => {
    let strings = str.split(' ');
    let obj = {};

    for (let i in strings) {
        obj[strings[i]] = ++obj[strings[i]] || 1;
    }

    return obj;
}

///// 6 /////
const fibonacciNumber = (index) => {
    index = index / 2;
    let numb = [0, 1];

    for (let i = 0; i < index * 2 - 2; i++) {
        numb.push(numb[i] + numb[i+1]);
    }

    return numb;
}

///// 7 /////
const compute = {
    rectanglePerimeter(sideOne, sideTwo) {
        return (sideOne + sideTwo) * 2;
    },
    rectangleSquare(sideOne, sideTwo) {
        return sideOne * sideTwo;
    },
    trianglePerimeter(sideOne, sideTwo, sideThree) {
        return sideOne + sideTwo + sideThree;
    },
    triangleSquare(sideOne, sideThree) {
        return (sideOne * sideThree) / 2;
    },
    circlePerimeter(sideOne) {
        return 2 * Math.PI * sideOne;
    },
    cirсleSquare(sideOne) {
        return (2 * Math.PI * sideOne * (sideOne * 2)) / 4;
    },
}
///// 8 /////
const calculationFactorial = (n) => {
    return (n != 1) ? n * calculationFactorial(n - 1) : 1;
}

///// 9 /////
const multiplesOfTwo = (n) => {
    let massN = n.toString().split('').map(Number);
    let multiplesOfTwoSum = 0;

    for (let i = 0; i < massN.length; i++) {
        if (massN[i] % 2 == 0) {
            multiplesOfTwoSum += massN[i];
        }
    }

    return multiplesOfTwoSum;
}

const multiplesOfThree = (n) => {
    let massN = n.toString().split('').map(Number);
    let multiplesOfThreeSum = 0;
    for (let i = 0; i < massN.length; i++) {
        if (massN[i] % 3 == 0) {
            multiplesOfThreeSum += massN[i];
        }
    }

    return multiplesOfThreeSum;
}

const sumPositiveNumbers = (num) => {
    let positiv = 0;
    for (let i = 0; i < num.length; i++) {
        if (num[i] > 0) {
            positiv += num[i];
        }
    }

    return positiv;
}

const oddNumbers = (n) => {
    let massN = n.toString().split('').map(Number)
    let sumOddNumbers = 0;

    for (let i = 0; i < massN.length; i++) {
        if (massN[i] % 2) {
            sumOddNumbers += massN[i];
        }
    }

    return sumOddNumbers;
}

///// 10 /////
const determinesNumberElementsArray = {
    defineZero(mass, zero) {
        zero = zero || 0;
        for (let i = 0; i < mass.length; i++) {
            if (mass[i] == 0) {
                zero++;
            }
        }

        return zero;
    },
    takesNegative(mass, negative) {
        negative = negative || 0;
        for (let i = 0; i < mass.length; i++) {
            if (mass[i] < 0) {
                negative++;
            }
        }

        return negative;
    },
    takesPositive(mass, positive) {
        positive = positive || 0;
        for (let i = 0; i < mass.length; i++) {
            if (mass[i] > 0) {
                positive++;
            }
        }

        return positive;
    },
    takesPrimeNumbers(mass, primeNumbers) {
        primeNumbers = primeNumbers || 0;
        for (let i = 0; i < mass.length; i++) {
            if (!(mass[i] % 2 && mass[i] % 3 && mass[i] % 5 && mass[i] % 7)) {
                primeNumbers++;
            }
        }

        return primeNumbers;
    }
}

///// 11 /////
const shiftingToBinary = (num) => {
    let out = [];
    let bit = 1;

    while (num >= bit) {
        out = (num & bit ? 1 : 0) + out;
        bit <<= 1;
    }

    return out || "0";
}

const shiftingToDecimal = (num) => {
    let out = 0;
    let len = num.length;
    let bit = 1;

    while (len--) {
        out += num[len] == 1 ? bit : 0;
        bit <<= 1;
    }

    return out;
}

///// 12 ////
const multiplesOfTwoDimensionalArray = (massN) => {
    let multiplesOfTwoSum = 0;

    for (let i = 0; i < massN.length; i++) {
        for (let j = 0; j < massN[i].length; j++) {
            if (massN[i][j] % 2 == 0) {
                multiplesOfTwoSum += massN[i][j];
            }
        }
    }

    return multiplesOfTwoSum;
}
const multiplesOfThreeDimensionalArray = (massN) => {
    let multiplesOfThreeSum = 0;

    for (let i = 0; i < massN.length; i++) {
        for (let j = 0; j < massN[i].length; j++) {
            if (massN[i][j] % 3 == 0) {
                multiplesOfThreeSum += massN[i][j];
            }
        }
    }

    return multiplesOfThreeSum;
}

const sumPositiveNumbersDimensionalArray = (num) => {
    let positiv = 0;

    for (let i = 0; i < num.length; i++) {
        for (let j = 0; j < num[i].length; j++) {
            if (num[i][j] > 0) {
                positiv += num[i][j];
            }
        }
    }

    return positiv;
}


const oddNumbersDimensionalArray = (massN) => {
    let sumOddNumbers = 0;

    for (let i = 0; i < massN.length; i++) {
        for (let j = 0; j < massN[i].length; j++) {
            if (massN[i][j] % 2) {
                sumOddNumbers += massN[i][j];
            }
        }
    }

    return sumOddNumbers;
}

const determinesTheNumberElementsTheArrayDimensionalArray = (mass) => {
    let objElems = {
        zero: 0,
        negative: 0,
        positive: 0,
        primeNumbers: 0,
    }

    for (let i = 0; i < mass.length; i++) {
        for (let j = 0; j < mass[i].length; j++) {
            if (mass[i][j] == 0) {
                objElems.zero++;
            }
            if (mass[i][j] < 0) {                
                objElems.negative++;
            }
            if (mass[i][j] > 0) {
                objElems.positive++;
            }
            if (!(mass[i][j] % 2 && mass[i][j] % 3 && mass[i][j] % 5 && mass[i][j] % 7)) {
                objElems.primeNumbers++;
            }
        }
    }

    return objElems;
}

///// 13 /////
const sumElementsMinMax = (min, max) => {
    let sumMinMax = 0;

    for (let i = min; i <= max; i++) {
        if (i % 3 == 0 && i > 0) {
            sumMinMax += i;
        }
    }

    return sumMinMax;
}

///// 14 /////
const theAverageOfArrayElement = (mass) => {
    let massFlat = mass.flat();
    let resultMean = {
        even: 0,
        notEven: 0,
    }

    for (let i = 0; i < massFlat.length; i++) {
        if (massFlat[i] % 2 == 0) {
            resultMean.even += massFlat[i] / 2;
        } else {
            resultMean.notEven += massFlat[i] / 2;
        }    }

    return resultMean;
}


///// 15 /////
const matrix = [
    [1, 2, 4, 45],
    [3, 4, 7, -2],
    [5, 6, 13, 54]
];

const transposedMatrix = (matrix) => matrix[0].map((x, i) => matrix.map(row => row[i]));



///// 16 /////
let matricesOne = [
    [1, 3, 7],    
    [3, 8, 2],
    [2, 9, 0]
];
let matricesTwo = [
    [1, 2, 2],
    [6, 2, 7],
    [1, 7, 9]
];

const addsTwoMatrices = (matricesOne, matricesTwo) => {
    let result = [        
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    for (let i = 0; i < matricesOne.length; i++) {
        for (let j = 0; j < matricesOne[i].length; j++) {
            result[i][j] += matricesOne[i][j] + matricesTwo[i][j];
        }
    }

    return result;
}


///// 17 /////
const removeFromArrayString = (mass) => {
    for (let i = 0; i < mass.length; i++) {
        for (let j = 0; j < mass[i].length; j++) {
            if (mass[i][j] == 0) {
                mass.splice(i, 1);
                break;
            }
        }
    }

    return mass;
}

const removeFromArrayСolumn = (mass) => {
    const deleteСolumn = (j) => {
        for (let i = 0; i < mass.length; i++) {
            mass[i].splice(j, 1);
        }
    }

    for (let i = 0; i < mass.length; i++) {
        for (let j = 0; j < mass[i].length; j++) {
            if (mass[i][j] == 0) {
                deleteСolumn(j);
                break;
            }
        }    }

    return mass;
}
