'use strict'
///// 3 /////
const numberCounterRecusrion = (nums, index, obj) => {
    index = index || 0;
    obj = obj || {};
    let mass_nums = nums.toString().split('');
    if (mass_nums.length > index) {
        obj[mass_nums[index]] = ++obj[mass_nums[index]] || 1;
        return numberCounterRecusrion(nums, ++index, obj);
    }

    return obj;
}

///// 4 /////
const numberOfUniqueWordsRecusrion = (str, index, obj) => {
    index = index || 0;
    obj = obj || {};
    let strings = str.replace(/\-/g, '').split(' ');

    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        return numberOfUniqueWordsRecusrion(str, ++index, obj);
    }
    let arrayOfValues = Object.values(obj);
    let counter = 0;

    for (let i = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] === 1) {
            counter += 1;
        }
    }

    return counter;
}

///// 5 /////
const numberOccurrenceOfEachWordRecusrion = (str, index, obj) => {
    index = index || 0;
    obj = obj || {};

    let strings = str.split(' ');
    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        numberOccurrenceOfEachWordRecusrion(str, ++index, obj);
    }
    return obj;
}

///// 6 /////
const fibonacciNumberRecusrion = (numberCycles, index, numbFibonacc) => {
    index = index || 0;
    numberCycles = numberCycles || 0;
    numbFibonacc = numbFibonacc || [0, 1];

    if (numberCycles / 2 > index) {
        numbFibonacc[0] = numbFibonacc[0] + numbFibonacc[1], numbFibonacc[1] = numbFibonacc[0] + numbFibonacc[1];
        fibonacciNumberRecusrion(numberCycles, ++index, numbFibonacc);
    }

    return numbFibonacc;
}

///// 8 /////
const calculationFactorialRecusrion = (n) => {
    return (n != 1) ? n * calculationFactorialRecusrion(n - 1) : 1;
}

///// 9 /////
const multiplesOfTwoRecusrion = (numb, index, resultSum) => {
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 2 == 0) {
            resultSum += massN[index];
            return multiplesOfTwoRecusrion(numb, ++index, resultSum);
        } else {
            resultSum = multiplesOfTwoRecusrion(numb, ++index, resultSum);
        }
    }

    return resultSum;
}

const multiplesOfThreeRecusrion = (numb, index, resultSum) => {
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 3 == 0) {
            resultSum += massN[index];
            return multiplesOfThreeRecusrion(numb, ++index, resultSum);
        } else {
            resultSum = multiplesOfThreeRecusrion(numb, ++index, resultSum);
        }
    }

    return resultSum;
}

const sumPositiveNumbersRecusrion = (numb, index, resultSum) => {
    index = index || 0;
    resultSum = resultSum || 0;
    if (numb.length > index) {
        if (numb[index] > 0) {
            resultSum += numb[index];
            return sumPositiveNumbersRecusrion(numb, ++index, resultSum);
        } else {
            resultSum = sumPositiveNumbersRecusrion(numb, ++index, resultSum);
        }
    }

    return resultSum;
}

const oddNumbersRecusrion = (numb, index, resultSum) => {
    index = index || 0;
    resultSum = resultSum || 0;
    let massN = numb.toString().split('').map(Number);
    if (massN.length > index) {
        if (massN[index] % 2) {
            resultSum += massN[index];
            return oddNumbersRecusrion(numb, ++index, resultSum);
        } else {
            resultSum = oddNumbersRecusrion(numb, ++index, resultSum);
        }
    }

    return resultSum;
}

///// 10 /////
const determinesElementsArrayRecusrion = {
    defineZero(mass, zero, index) {
        zero = zero || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] == 0) {
                zero++;
                return determinesElementsArrayRecusrion.defineZero(mass, zero, ++index);
            } else {
                zero = determinesElementsArrayRecusrion.defineZero(mass, zero, ++index);
            }
        }

        return zero;
    },

    takesNegative(mass, negative, index) {
        negative = negative || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] < 0) {
                negative++;
                return determinesElementsArrayRecusrion.takesNegative(mass, negative, ++index);
            } else {
                negative = determinesElementsArrayRecusrion.takesNegative(mass, negative, ++index);
            }
        }

        return negative;
    },
    takesPositive(mass, positive, index) {
        positive = positive || 0;
        index = index || 0;
        if (mass.length > index) {
            if (mass[index] > 0) {
                positive++;
                return determinesElementsArrayRecusrion.takesPositive(mass, positive, ++index);
            } else {
                positive = determinesElementsArrayRecusrion.takesPositive(mass, positive, ++index);
            }
        }

        return positive;
    },
    takesPrimeNumbers(mass, primeNumbers, index) {
        primeNumbers = primeNumbers || 0;
        index = index || 0;
        if (mass.length > index) {
            if (!(mass[index] % 2 && mass[index] % 3 && mass[index] % 5 && mass[index] % 7)) {
                primeNumbers++;
                return determinesElementsArrayRecusrion.takesPrimeNumbers(mass, primeNumbers, ++index);
            } else {
                primeNumbers = determinesElementsArrayRecusrion.takesPrimeNumbers(mass, primeNumbers, ++index);
            }
        }

        return primeNumbers;
    }
}

///// 12 ////
const multiplesOfTwoDimensionalArrayRecusrion = (mass) => {
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 2 == 0) {
            result += pavedMassif[index];
        }
    }

    return result;
}

const multiplesOfThreeDimensionalArrayRecusrion = (mass) => {
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 3 == 0 && pavedMassif[index] !== 0) {
            result += pavedMassif[index];
        }
    }
    return result;
}

const sumPositiveNumbersDimensionalArrayRecusrion = (mass) => {
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);
    for (let index in pavedMassif) {
        if (pavedMassif[index] > 0) {
            result += pavedMassif[index];
        }
    }
    return result;
}

const oddNumbersDimensionalArrayRecusrion = (mass) => {
    let pavedMassif = [];
    let result = 0;
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] % 2) {
            result += pavedMassif[index];
        }
    }
    return result;
}

const determinesTheNumberElementsTheArrayDimensionalArrayRecusrion = (mass) => {
    let objElems = {
        zero: 0,
        negative: 0,
        positive: 0,
        primeNumbers: 0,
    }
    let pavedMassif = [];
    const parseArray = (mass) => {
        for (let item in mass) {
            if (typeof mass[item] === "object") {
                parseArray(mass[item]);
            } else {
                pavedMassif.push(mass[item]);
            }
        }
    }
    parseArray(mass);

    for (let index in pavedMassif) {
        if (pavedMassif[index] == 0) {
            objElems.zero++;
        }
        if (pavedMassif[index] < 0) {
            objElems.negative++;
        }
        if (pavedMassif[index] > 0) {
            objElems.positive++;
        }
        if (!(pavedMassif[index] % 2 && pavedMassif[index] % 3 && pavedMassif[index] % 5 && pavedMassif[index] % 7)) {
            objElems.primeNumbers++;
        }
    }

    return objElems;
}

///// 13 /////
const sumElementsMinMaxRecusrion = (min, max, sumMinMax) => {
    sumMinMax = sumMinMax || 0;
    if (++min <= max) {
        if (min % 3 == 0 && min > 0) {
            sumMinMax += min;
        }
        sumMinMax = sumElementsMinMaxRecusrion(min, max, sumMinMax);
    }

    return sumMinMax;
}

///// 14 /////
const theAverageOfArrayElementRecusrion = (mass) => {
    let massFlat = mass.flat();
    let resultMean = {
        even: 0,
        notEven: 0,
    }

    const parsingArray = (massFlat, counter) => {
        counter = counter || 0;
        if (massFlat.length > counter) {
            if (massFlat[counter] % 2 == 0) {
                resultMean.even += massFlat[counter] / 2;
            } else {
                resultMean.notEven += massFlat[counter] / 2;
            }

            parsingArray(massFlat, ++counter);
        }
    }
    parsingArray(massFlat);

    return resultMean;
}

///// 16 /////
let matricesOneRecursion = [
    [1, 3, 7],
    [3, 8, 2],
    [2, 9, 0]
];

let matricesTwoRecursion = [
    [1, 2, 2],
    [6, 2, 7],
    [1, 7, 9]
];

const addsTwoMatricesRecusrion = (matricesOneRecursion, matricesTwo, count, result) => {
    count = count || 0;
    result = result || [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    if (count < matricesOneRecursion.length) {
        for (let index in matricesOneRecursion[count]) {
            result[count][index] += matricesOneRecursion[count][index] + matricesTwoRecursion[count][index];
        }

        addsTwoMatricesRecusrion(matricesOneRecursion, matricesTwoRecursion, ++count, result);
    }

    return result;
}

///// 17 /////
const removeFromArrayStringRecusrion = (mass, index) => {
    index = index || 0;

    if (index++ < mass.length) {
        for (let j in mass[index]) {
            if (mass[index][j] == 0) {
                mass.splice(index, 1);
                --index;
                break;
            }
        }

        removeFromArrayStringRecusrion(mass, index);
    }

    return mass;
}

const removeFromArrayСolumnRecusrion = (mass, index) => {
    index = index || 0
    const deleteСolumn = (j) => {
        for (let index in mass) {
            mass[index].splice(j, 1);
        }
    }

    if (index++ < mass.length) {
        for (let j in mass[index]) {
            if (mass[index][j] == 0) {
                deleteСolumn(j);
                break;
            }
        }

        removeFromArrayСolumnRecusrion(mass, index);
    }

    return mass;
}
