'use strict'
Array.prototype.myReduce = function (fun, accumulytor) {
    accumulytor = accumulytor || this[0];

    for (let i = 0; i < this.length; i++) {
        accumulytor = fun(accumulytor, this[i], i, this);
    }

    return accumulytor;
}

Array.prototype.myFilter = function (compare) {
    let result = [];

    for (let i = 0; i < this.length; i++) {
        if (compare(this[i])) {
            result.push(this[i]);
        }
    }

    return result;
}

Array.prototype.myForEach = function (compare, index) {
    index = index || 0;

    if (index < this.length) {
        compare(this[index], this);
        this.myForEach(compare, ++index);
    }
};

let arr = [{
        name: 'Vasia',
        age: 34,
    },
    {
        name: 'Vlad',
        age: 31,
    },
    {
        name: 'Igor',
        age: 20,
    },
    {
        name: 'Svetlana',
        age: 19,
    },
    {
        name: 'Vasia',
        age: 22,
    },
    {
        name: 'Alex',
        age: 40,
    },
]

Array.prototype.myMap = function (compare) {
    let result = [];

    for (let i = 0; i < this.length; i++) {
        result.push(compare(this[i]));
    }

    return result;
}

let object = {
    a: 100,
    b: 10,
}

let testFun = function (a, b, c) {
    return this.a + this.b + a + b + c;
}

Function.prototype.myBind = function (context, ...args) {
    let object = {
        ...context,
    };
    let symbol = Symbol();
    object[symbol] = this;

    return function (...rest) {
        let result = object[symbol](...args, ...rest);
        delete object[symbol];
        return result;
    };
};

Function.prototype.myCall = function (object, ...args) {
    let symbol = Symbol();
    object[symbol] = this;
    let result = object[symbol](...args);
    delete object[symbol];
    return result;
}

let testSum = function (a, b) {
    return this.a + this.b + a + b;
};

let fibonacciNumberIterator = {
    min: 0,
    max: 10,
    [Symbol.iterator]() {
        let current = this.min;
        let max = this.max;
        let numb = [0, 1];
        let result = 0;
        return {
            next() {
                if (current >= max) {
                    result = undefined;
                } else {
                    numb.push(numb[current] + numb[current + 1]);
                    result = numb[current];
                }
                current++;

                return {
                    value: result,
                    done: current > max,
                };
            }
        };
    },
};

function* fibonachi(n, current, next) {
    current = current || 0;
    next = next || 1;

    if (n == 0) {
        return current;
    }

    yield current;
    yield* fibonachi(n - 1, next, current + next);
}
